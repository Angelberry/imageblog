const express = require("express");
const User = require("../models/user");
const bcrypt = require("bcrypt-nodejs");
const jwt = require("jsonwebtoken");

const router = express.Router();

router.post("/signup", (req, res, next) => {
  bcrypt.hash(req.body.password, null, null, (err, hash) => {
    if (err) {
      return res.status(500).json({
        message: "Issue in creating hash for password",
        error: err
      })
    }
    const user = new User({
      email: req.body.email,
      password: hash
    })
    user.save()
      .then(result => {
        res.status(201).json({
          message: "New user created!",
          result: result
        });
      })
      .catch(err => {
        res.status(500).json({
          error: err
        });
      });
  });
});

router.post("/login", (req, res, next) => {
  User.findOne({
    email: req.body.email
  }).then(user => {
    if (!user) {
      return res.status(401).json({
        message: "Auth Failed"
      });
    }
    return bcrypt.compare(req.body.password, user.password, (err, result) => {
      if (!result || err) {
        return res.status(401).json({
          message: "Auth Failed"
        });
      }
      const token = jwt.sign({
        email: user.email,
        userId: user._id
      }, "secret_this_should_be_longer", {
        expiresIn: "1h"
      });
      res.status(200).json({
        token: token,
        expiresIn: 3600,
        userId: user._id
      })
    });
  });
});



module.exports = router;
